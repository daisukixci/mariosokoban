/**
*
*2013-03-24-17.44.46
*
*@author : Piquenot Ga�tan
*mail : piquenot.gaetan@gmail.com
*
*
*UTILITY: Put prototype for the main and inclusion of all files cpp and h and define constant for all the project
*
*/

#ifndef DEF_MAIN
#define DEF_MAIN

//*****INCLUDE*****
#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "menu.h"


#endif
