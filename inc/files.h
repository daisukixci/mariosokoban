/**
*
*2013-04-19-16.49.21
*
*@author : Piquenot Ga�tan
*mail : piquenot.gaetan@gmail.com
*
*
*UTILITY:
*
*/

#ifndef DEF_FILES
#define DEF_FILES

//include
#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "constant.h"
//define

//prototype
int loadLevel(int level[][NB_BLOCKS_HEIGHT],int levelNumber);
int saveLevel(int level[][NB_BLOCKS_HEIGHT],int levelNumber);
#endif
