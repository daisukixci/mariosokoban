/**
*
*2013-04-19-16.49.30
*
*@author : Piquenot Ga�tan
*mail : piquenot.gaetan@gmail.com
*
*
*UTILITY: Define constant for all the program
*
*/

#ifndef DEF_CONSTANT
#define DEF_CONSTANT

//*****INCLUDE*****

//*****DEFINE*****
#define SIZE_BLOCK 34
#define NB_BLOCKS_WIDTH 12
#define NB_BLOCKS_HEIGHT 12
#define WINDOW_WIDTH SIZE_BLOCK*NB_BLOCKS_WIDTH
#define WINDOW_HEIGHT SIZE_BLOCK*NB_BLOCKS_HEIGHT
#define LVLMAX 20
#define LVLSIZE 146


//*****PROTOTYPE*****
enum {UP,DOWN,RIGHT,LEFT};
enum {EMPTY,WALL,BOX,GOAL,MARIO,BOX_OK};

#endif
