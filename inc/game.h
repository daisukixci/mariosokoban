/**
*
*2013-04-19-16.49.17
*
*@author : Piquenot Ga�tan
*mail : piquenot.gaetan@gmail.com
*
*
*UTILITY: Put prototype for the main
*
*/

#ifndef DEF_GAME
#define DEF_GAME

//*****INCLUDE*****
#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "files.h"
#include "constant.h"


//*****PROTOTYPE*****
void play(SDL_Surface *screen);
void moveMario(int map[][NB_BLOCKS_HEIGHT],SDL_Rect *marioPosition,int direction);
void moveBox(int *firstCase, int *secondCase);
void wellDone(SDL_Surface *screen);
#endif
