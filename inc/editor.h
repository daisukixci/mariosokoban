/**
*
*2013-04-19-16.49.26
*
*@author : Piquenot Ga�tan
*mail : piquenot.gaetan@gmail.com
*
*
*UTILITY: Put prototype for the main
*
*/

#ifndef DEF_EDITOR
#define DEF_EDITOR

//include
#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "constant.h"
#include "files.h"
//define

//prototype
void editor(SDL_Surface *screen,int levelNumber);
#endif
