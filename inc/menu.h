/**
*
*2013-03-24-14.20.52
*
*@author : Piquenot Ga�tan
*mail : piquenot.gaetan@gmail.com
*
*
*UTILITY: Put prototype for the main
*
*/

#ifndef DEF_MENU
#define DEF_MENU

//*****INCLUDE*****
#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "game.h"
#include "constant.h"
#include "editor.h"
//*****DEFINE*****

//*****PROTOTYPE*****
void menu(SDL_Surface *screen);
void tutoGame(SDL_Surface *screen);
void tutoEdit(SDL_Surface *screen);
void congratulations(SDL_Surface *screen);

#endif
