NAME=mariosokoban
DEBUG=yes
CC=gcc
VERSION=0.1
BIN=$(NAME)-$(VERSION)
OBJDIR=obj
SRCDIR=src
INCDIR=inc
DEBUGDIR=bin/debug
RELEASEDIR=bin/release
ifeq ($(DEBUG),yes)
	CFLAGS=-W -Wall -std=c99 -pedantic -g
	LDFLAGS=-pthread -lSDL_image
	EXEC=$(DEBUGDIR)/$(BIN)
else
	CFLAGS=-W -Wall -std=c99 -pedantic
	LDFLAGS=-pthread -lSDL_image
	EXEC=$(RELEASEDIR)/$(BIN)
endif
SDL_CFLAGS=$(shell sdl-config --cflags --libs)
SRC=$(wildcard $(SRCDIR)/*.c)
_OBJ=$(SRC:.c=.o)
OBJ=$(patsubst $(SRCDIR)/%,$(OBJDIR)/%,$(_OBJ))
INC=$(wildcard $(INCDIR)/*.h)

all: $(EXEC)
ifeq ($(DEBUG),yes)
	@echo "Mario sokoban generated in debug"
else
	@echo "Mario sokoban generated in prod"
endif

$(EXEC):$(OBJ)
	@echo "Game in building ..."
	@$(CC) $^ $(LDFLAGS) $(CFLAGS) $(SDL_CFLAGS) -o $@

$(OBJDIR)/main.o: $(INC)

$(OBJDIR)/%.o: $(SRCDIR)/%.c $(INC)
	@echo "A game piece is in progress"
	@$(CC) -c $< $(CFLAGS) $(SDL_CFLAGS) -o $@
	@echo "A game piece is finished"

.PHONY: clean purge

clean:
	@rm -rf obj/*.o
	@echo "Cleaned"

purge: clean
	@rm -rf $(EXEC)
	@echo "Purged"
