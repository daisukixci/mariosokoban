/**
*
*2013-03-24-18.44.53u
*
*@author : Piquenot Ga�tan
*mail : piquenot.gaetan@gmail.com
*
*
*UTILITY: define constant for all the program
*
*/

#include "../inc/main.h"

int main ( int argc, char** argv )
{
    //*****VARIABLES DECLARATION*****
    SDL_Surface *screen=NULL;

    //*****INITIALIZATION OF VIDEO*****
    if(SDL_Init(SDL_INIT_VIDEO)==-1)
    {
        fprintf(stderr,"Failure of video initialization: %s\n",SDL_GetError());
        exit(EXIT_FAILURE);
    }
    putenv("SDL_VIDEO_WINDOW_POS=center");
    SDL_WM_SetIcon(IMG_Load("picture/ico/pipe.png"),NULL);

    //*****INITIALIZATION OF VIDEOMODE*****
    screen=SDL_SetVideoMode(WINDOW_WIDTH,WINDOW_HEIGHT,32,SDL_HWSURFACE|SDL_DOUBLEBUF);
    if(screen==NULL)
    {
        fprintf(stderr,"Failure of SetVideoMode initialization: %s\n",SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_WM_SetCaption("Mario Sokoban",NULL);

    //*****BEGINING OF GAME*****
    menu(screen);

    SDL_Quit();
    return EXIT_SUCCESS;
}

