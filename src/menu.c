/**
*
*2013-03-24-14.20.49
*
*@author : Piquenot Ga�tan
*mail : piquenot.gaetan@gmail.com
*
*
*UTILITY: Create an interactive menu to choice between play, editor and exit
*
*/

//*****INCLUDE*****
#include "../inc/menu.h"

//*****FUNCTIONS*****
void menu(SDL_Surface *screen)
{
    //*****VARIABLES DECLARATION*****
    //Picture declaration
    SDL_Surface *wallpaper=NULL;
    SDL_Rect wallpaperPosition;
    wallpaperPosition.x=0;
    wallpaperPosition.y=0;
    SDL_Surface *title=NULL;
    SDL_Rect titlePosition;
    titlePosition.x=0;
    titlePosition.y=0;
    SDL_Surface *playButton=NULL;
    SDL_Rect playButtonPosition;
    playButtonPosition.x=0;
    playButtonPosition.y=0;
    SDL_Surface *editorButton=NULL;
    SDL_Rect editorButtonPosition;
    editorButtonPosition.x=0;
    editorButtonPosition.y=0;
    SDL_Surface *exitButton=NULL;
    SDL_Rect exitButtonPosition;
    exitButtonPosition.x=0;
    exitButtonPosition.y=0;
    //Event declaration
    int boolean=1;
    SDL_Event event;

    wallpaper=IMG_Load("picture/menu/wallpaper.png");
    title=IMG_Load("picture/menu/title.png");
    titlePosition.x=screen->w/2-title->w/2;
    titlePosition.y=0;
    playButton=IMG_Load("picture/menu/play_off.png");
    playButtonPosition.x=screen->w/2-playButton->w/2;
    playButtonPosition.y=screen->h/2-playButton->h/0.5;
    editorButton=IMG_Load("picture/menu/editor_off.png");
    editorButtonPosition.x=screen->w/2-editorButton->w/2;
    editorButtonPosition.y=screen->h/2-editorButton->h/2;
    exitButton=IMG_Load("picture/menu/exit_off.png");
    exitButtonPosition.x=screen->w/2-exitButton->w/2;
    exitButtonPosition.y=screen->h/2+exitButton->h;

    while(boolean)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                boolean=0;
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        boolean=0;
                        break;
                    default:
                        break;
                }
            //for the fly of button
            case SDL_MOUSEMOTION:
                //if the mouse is on playButton
                if((event.motion.y>playButtonPosition.y && event.motion.y<playButtonPosition.y+playButton->h)&&(event.motion.x>playButtonPosition.x && event.motion.x<playButtonPosition.x+playButton->w))
                {
                    playButton=IMG_Load("picture/menu/play_on.png");
                }
                else
                {
                    playButton=IMG_Load("picture/menu/play_off.png");
                }
                //if the mouse is on editorButton
                if((event.motion.y>editorButtonPosition.y && event.motion.y<editorButtonPosition.y+editorButton->h)&&(event.motion.x>editorButtonPosition.x && event.motion.x<editorButtonPosition.x+editorButton->w))
                {
                    editorButton=IMG_Load("picture/menu/editor_on.png");
                }
                else
                {
                    editorButton=IMG_Load("picture/menu/editor_off.png");
                }
                //if the mouse is on exitButton
                if((event.motion.y>exitButtonPosition.y && event.motion.y<exitButtonPosition.y+exitButton->h)&&(event.motion.x>exitButtonPosition.x && event.motion.x<exitButtonPosition.x+exitButton->w))
                {
                    exitButton=IMG_Load("picture/menu/exit_on.png");
                }
                else
                {
                    exitButton=IMG_Load("picture/menu/exit_off.png");
                }
                break;
            case SDL_MOUSEBUTTONDOWN:
                //if the mouse is on playButton and you click
                if(event.motion.y>playButtonPosition.y && event.motion.y<playButtonPosition.y+playButton->h)
                {
                    if(event.motion.x>playButtonPosition.x && event.motion.x<playButtonPosition.x+playButton->w)
                    {
                        if(event.button.button==SDL_BUTTON_LEFT)
                        {
                            tutoGame(screen);
                            play(screen);
                        }
                    }
                }
                //if the mouse is on editorButton
                if(event.motion.y>editorButtonPosition.y && event.motion.y<editorButtonPosition.y+editorButton->h)
                {
                    if(event.motion.x>editorButtonPosition.x && event.motion.x<editorButtonPosition.x+editorButton->w)
                    {
                        if(event.button.button==SDL_BUTTON_LEFT)
                        {
                            tutoEdit(screen);
                            editor(screen,1);
                        }
                    }
                }
                //if the mouse is on exitButton
                if(event.motion.y>exitButtonPosition.y && event.motion.y<exitButtonPosition.y+exitButton->h)
                {
                    if(event.motion.x>exitButtonPosition.x && event.motion.x<exitButtonPosition.x+exitButton->w)
                    {
                        if(event.button.button==SDL_BUTTON_LEFT)
                        {
                            boolean=0;
                        }
                    }
                }
                break;
            default:
                break;
        }
        SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,255,255,255));
        //All BlitSurface display
        SDL_BlitSurface(wallpaper,NULL,screen,&wallpaperPosition);
        SDL_BlitSurface(title,NULL,screen,&titlePosition);
        SDL_BlitSurface(playButton,NULL,screen,&playButtonPosition);
        SDL_BlitSurface(editorButton,NULL,screen,&editorButtonPosition);
        SDL_BlitSurface(exitButton,NULL,screen,&exitButtonPosition);
        SDL_Flip(screen);
    }
    SDL_FreeSurface(exitButton);
    SDL_FreeSurface(editorButton);
    SDL_FreeSurface(playButton);
    SDL_FreeSurface(title);
    SDL_FreeSurface(wallpaper);
}


void tutoGame(SDL_Surface *screen)
{
    //*****VARIABLES DECLARATION*****
    SDL_Surface *wallpaper=NULL;
    SDL_Rect wallpaperPosition;
    wallpaperPosition.x=0;
    wallpaperPosition.y=0;
    SDL_Surface *tutorial=NULL;
    SDL_Rect tutoPosition;
    tutoPosition.x=0;
    tutoPosition.y=0;
    wallpaper=IMG_Load("picture/menu/wallpaper.png");
    tutorial=IMG_Load("picture/menu/tutoGame.png");
    int boolean=1;
    SDL_Event event;

    while(boolean)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                exit(EXIT_SUCCESS);
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_RETURN:
                        boolean=0;
                        break;
                    default:
                        break;
                }
            default:
                break;
        }
        SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,255,255,255));
        SDL_BlitSurface(wallpaper,NULL,screen,&wallpaperPosition);
        SDL_BlitSurface(tutorial,NULL,screen,&tutoPosition);
        SDL_Flip(screen);
    }
    SDL_FreeSurface(wallpaper);
    SDL_FreeSurface(tutorial);
}

void tutoEdit(SDL_Surface *screen)
{
    //*****VARIABLES DECLARATION*****
    SDL_Surface *wallpaper=NULL;
    SDL_Rect wallpaperPosition;
    wallpaperPosition.x=0;
    wallpaperPosition.y=0;
    SDL_Surface *tutorial=NULL;
    SDL_Rect tutoPosition;
    tutoPosition.x=0;
    tutoPosition.y=0;
    wallpaper=IMG_Load("picture/menu/wallpaper.png");
    tutorial=IMG_Load("picture/menu/tutoEdit.png");
    int boolean=1;
    SDL_Event event;

    while(boolean)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                exit(EXIT_SUCCESS);
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_RETURN:
                        boolean=0;
                        break;
                    default:
                        break;
                }
            default:
                break;
        }
        SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,255,255,255));
        SDL_BlitSurface(wallpaper,NULL,screen,&wallpaperPosition);
        SDL_BlitSurface(tutorial,NULL,screen,&tutoPosition);
        SDL_Flip(screen);
    }
    SDL_FreeSurface(wallpaper);
    SDL_FreeSurface(tutorial);
}

void congratulations(SDL_Surface *screen)
{
    int boolean=1;
    SDL_Surface *congratulation=NULL;
    SDL_Rect congratulationPosition;
    congratulationPosition.x=0;
    congratulationPosition.y=0;
    SDL_Surface *wallpaper=NULL;
    SDL_Rect wallpaperPosition;
    wallpaperPosition.x=0;
    wallpaperPosition.y=0;
    SDL_Event event;
    congratulation=IMG_Load("picture/menu/congrats.png");
    wallpaper=IMG_Load("picture/menu/wallpaper.png");

    while(boolean)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_RETURN:
                        boolean=0;
                        break;
                    case SDLK_ESCAPE:
                        boolean=0;
                        break;
                    default:
                        break;
                }
            default:
                break;
        }
        SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,255,255,255));
        SDL_BlitSurface(wallpaper,NULL,screen,&wallpaperPosition);
        SDL_BlitSurface(congratulation,NULL,screen,&congratulationPosition);
        SDL_Flip(screen);
    }
    SDL_FreeSurface(congratulation);
    SDL_FreeSurface(wallpaper);
}
