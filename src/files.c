/**
*
*nowlu
*
*@author : Piquenot Ga�tan
*mail : piquenot.gaetan@gmail.com
*
*
*UTILITY: Load and save level
*
*/
#include "../inc/files.h"

int loadLevel(int level[][NB_BLOCKS_HEIGHT],int levelNumber)
{
    FILE* file = NULL;
    char lineFile[NB_BLOCKS_WIDTH * NB_BLOCKS_HEIGHT + 1] = {0};
    int i = 0, j = 0;
    file = fopen("level/level.lvl", "r");
    if (file == NULL)
        return 0;
    switch(levelNumber)
    {
        case 1:
            rewind(file);
            break;
        case 2:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 3:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 4:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 5:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 6:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 7:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 8:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 9:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 10:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 11:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 12:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 13:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 14:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 15:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 16:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 17:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 18:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 19:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 20:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        default:
            break;
    }
    if(fgets(lineFile, NB_BLOCKS_WIDTH * NB_BLOCKS_HEIGHT + 1,file)==NULL)
        exit(EXIT_FAILURE);
    for (i = 0 ; i < NB_BLOCKS_WIDTH ; i++)
    {
        for (j = 0 ; j < NB_BLOCKS_HEIGHT ; j++)
        {
            switch (lineFile[(i * NB_BLOCKS_WIDTH) + j])
            {
                case '0':
                    level[j][i] = 0;
                    break;
                case '1':
                    level[j][i] = 1;
                    break;
                case '2':
                    level[j][i] = 2;
                    break;
                case '3':
                    level[j][i] = 3;
                    break;
                case '4':
                    level[j][i] = 4;
                    break;
            }
        }
    }
    rewind(file);
    fclose(file);
    return 1;
}


int saveLevel(int map[][NB_BLOCKS_HEIGHT],int levelNumber)
{
    FILE* file = NULL;
    int i = 0, j = 0;
    file = fopen("level/level.lvl", "r+");
    if (file == NULL)
        return 0;
    switch(levelNumber)
    {
        case 1:
            rewind(file);
            break;
        case 2:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 3:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 4:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 5:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 6:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 7:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 8:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 9:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 10:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 11:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 12:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 13:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 14:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 15:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 16:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 17:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 18:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 19:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        case 20:
            fseek(file,(levelNumber-1)*LVLSIZE,SEEK_SET);
            break;
        default:
            break;
    }
    for (i = 0 ; i < NB_BLOCKS_WIDTH ; i++)
    {
        for (j = 0 ; j < NB_BLOCKS_HEIGHT ; j++)
        {
            fprintf(file, "%d", map[j][i]);
        }
    }
    fprintf(file,"\n");
    rewind(file);
    fclose(file);
    return 1;
}

