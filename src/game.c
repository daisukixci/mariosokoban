/**
*
*nowlu
*
*@author : Piquenot Ga�tan
*mail : piquenot.gaetan@gmail.com
*
*
*UTILITY: define constant for all the program
*
*/

#include "../inc/game.h"

void play(SDL_Surface *screen)
{
    //*****VARIABLES DECLARATION*****
    //Picture declaration
    SDL_Surface *mario[4]={NULL};
    SDL_Rect marioPosition;
    SDL_Surface *levelDisplay[20]={NULL};
    SDL_Surface *levelBlit=levelDisplay[0];
    SDL_Rect levelBlitPosition;
    levelBlitPosition.x=0;
    levelBlitPosition.y=0;
    SDL_Surface *wall = NULL, *box = NULL, *boxOK = NULL, *goal= NULL, *marioActual = NULL;
    SDL_Rect position;
    //Event declaration
    int boolean=1,remainingGoals=0,i=0,j=0,quit=1;
    int levelNumber=1;
    int map[NB_BLOCKS_WIDTH][NB_BLOCKS_HEIGHT];
    for(i=0;i<NB_BLOCKS_WIDTH;i++)
    {
        for(j=0;j<NB_BLOCKS_HEIGHT;j++)
        {
            map[i][j]=0;
        }
    }
    SDL_Event event;
    SDL_EnableKeyRepeat(100, 100);
    SDL_ShowCursor(SDL_DISABLE);

    //Load sprites
    wall=IMG_Load("picture/sprites/wall.jpg");
    box=IMG_Load("picture/sprites/box.jpg");
    boxOK=IMG_Load("picture/sprites/box_ok.jpg");
    goal=IMG_Load("picture/sprites/goal.png");
    mario[UP]=IMG_Load("picture/char/mario_up.gif");
    mario[DOWN]=IMG_Load("picture/char/mario_down.gif");
    mario[LEFT]=IMG_Load("picture/char/mario_left.gif");
    mario[RIGHT]=IMG_Load("picture/char/mario_right.gif");
    levelDisplay[0]=IMG_Load("picture/menu/level1.png");
    levelDisplay[1]=IMG_Load("picture/menu/level2.png");
    levelDisplay[2]=IMG_Load("picture/menu/level3.png");
    levelDisplay[3]=IMG_Load("picture/menu/level4.png");
    levelDisplay[4]=IMG_Load("picture/menu/level5.png");
    levelDisplay[5]=IMG_Load("picture/menu/level6.png");
    levelDisplay[6]=IMG_Load("picture/menu/level7.png");
    levelDisplay[7]=IMG_Load("picture/menu/level8.png");
    levelDisplay[8]=IMG_Load("picture/menu/level9.png");
    levelDisplay[9]=IMG_Load("picture/menu/level10.png");
    levelDisplay[10]=IMG_Load("picture/menu/level11.png");
    levelDisplay[11]=IMG_Load("picture/menu/level12.png");
    levelDisplay[12]=IMG_Load("picture/menu/level13.png");
    levelDisplay[13]=IMG_Load("picture/menu/level14.png");
    levelDisplay[14]=IMG_Load("picture/menu/level15.png");
    levelDisplay[15]=IMG_Load("picture/menu/level16.png");
    levelDisplay[16]=IMG_Load("picture/menu/level17.png");
    levelDisplay[17]=IMG_Load("picture/menu/level18.png");
    levelDisplay[18]=IMG_Load("picture/menu/level19.png");
    levelDisplay[19]=IMG_Load("picture/menu/level20.png");
    marioActual=mario[DOWN];


    while(levelNumber!=LVLMAX+1 && quit==1)
    {
        //Load level
        if(!loadLevel(map,levelNumber))
            exit(EXIT_FAILURE);

        //Search the spawn case
        for(i=0;i<NB_BLOCKS_WIDTH;i++)
        {
            for(j=0;j<NB_BLOCKS_HEIGHT;j++)
            {
                if(map[i][j]==MARIO)
                {
                    marioPosition.x=i;
                    marioPosition.y=j;
                    map[i][j]=EMPTY;
                }
            }
        }
        boolean=1;
        //Main loop to react at event
        while(boolean)
        {
            SDL_WaitEvent(&event);
            switch(event.type)
            {
                // to quit the game
                case SDL_QUIT:
                    exit(EXIT_SUCCESS);
                    break;
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym)
                    {
                        case SDLK_ESCAPE:
                            quit=0;
                            boolean=0;
                            break;
                        case SDLK_r:
                            if(!loadLevel(map,levelNumber))
                                exit(EXIT_FAILURE);
                            for(i=0;i<NB_BLOCKS_WIDTH;i++)
                            {
                                for(j=0;j<NB_BLOCKS_HEIGHT;j++)
                                {
                                    if(map[i][j]==MARIO)
                                    {
                                        marioPosition.x=i;
                                        marioPosition.y=j;
                                        map[i][j]=EMPTY;
                                    }
                                }
                            }
                            break;
                        case SDLK_UP:
                            marioActual=mario[UP];
                            moveMario(map,&marioPosition,UP);
                            break;
                        case SDLK_DOWN:
                            marioActual=mario[DOWN];
                            moveMario(map,&marioPosition,DOWN);
                            break;
                        case SDLK_LEFT:
                            marioActual=mario[LEFT];
                            moveMario(map,&marioPosition,LEFT);
                            break;
                        case SDLK_RIGHT:
                            marioActual=mario[RIGHT];
                            moveMario(map,&marioPosition,RIGHT);
                            break;
                        default:
                            break;
                    }
            }
            SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,255,255,255));
            //All BlitSurface display
            remainingGoals=0;
            for(i=0;i<NB_BLOCKS_WIDTH;i++)
            {
                for(j=0;j<NB_BLOCKS_HEIGHT;j++)
                {
                    position.x=i*SIZE_BLOCK;
                    position.y=j*SIZE_BLOCK;
                    switch(map[i][j])
                    {
                        case WALL:
                            SDL_BlitSurface(wall,NULL,screen,&position);
                            break;
                        case BOX:
                            SDL_BlitSurface(box,NULL,screen,&position);
                            break;
                        case BOX_OK:
                            SDL_BlitSurface(boxOK,NULL,screen,&position);
                            break;
                        case GOAL:
                            SDL_BlitSurface(goal,NULL,screen,&position);
                            remainingGoals=1;
                            break;
                    }
                }
            }
            switch(levelNumber)
            {
                case 1:
                    levelBlit=levelDisplay[0];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 2:
                    levelBlit=levelDisplay[1];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 3:
                    levelBlit=levelDisplay[2];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 4:
                    levelBlit=levelDisplay[3];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 5:
                    levelBlit=levelDisplay[4];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 6:
                    levelBlit=levelDisplay[5];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 7:
                    levelBlit=levelDisplay[6];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 8:
                    levelBlit=levelDisplay[7];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 9:
                    levelBlit=levelDisplay[8];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 10:
                    levelBlit=levelDisplay[9];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 11:
                    levelBlit=levelDisplay[10];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 12:
                    levelBlit=levelDisplay[11];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 13:
                    levelBlit=levelDisplay[12];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 14:
                    levelBlit=levelDisplay[13];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 15:
                    levelBlit=levelDisplay[14];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 16:
                    levelBlit=levelDisplay[15];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 17:
                    levelBlit=levelDisplay[16];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 18:
                    levelBlit=levelDisplay[17];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 19:
                    levelBlit=levelDisplay[18];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 20:
                    levelBlit=levelDisplay[19];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                default:
                    break;
            }
            //victory test
            if(!remainingGoals)
            {
                levelNumber++;
                boolean=0;
                wellDone(screen);
            }
            //blit of mario
            position.x= marioPosition.x*SIZE_BLOCK;
            position.y= marioPosition.y*SIZE_BLOCK;
            SDL_BlitSurface(marioActual,NULL,screen,&position);
            SDL_Flip(screen);
        }
    }

    SDL_EnableKeyRepeat(0,0);
    SDL_ShowCursor(SDL_ENABLE);
    SDL_FreeSurface(wall);
    SDL_FreeSurface(box);
    SDL_FreeSurface(boxOK);
    SDL_FreeSurface(goal);
    for(i=0;i<4;i++)
        SDL_FreeSurface(mario[i]);
    for(i=0;i<20;i++)
        SDL_FreeSurface(levelDisplay[i]);
}

void moveMario(int map[][NB_BLOCKS_HEIGHT],SDL_Rect *marioPosition,int direction)
{
    //test if the move is possible
    switch(direction)
    {
        case UP:
            if(marioPosition->y-1<0)
                break;
            if(map[marioPosition->x][marioPosition->y-1]==WALL)
                break;
            if ((map[marioPosition->x][marioPosition->y - 1] == BOX || map[marioPosition->x][marioPosition->y -1] == BOX_OK) &&(marioPosition->y - 2 < 0 || map[marioPosition->x][marioPosition->y - 2] == WALL || map[marioPosition->x][marioPosition->y - 2] == BOX || map[marioPosition->x][marioPosition->y - 2]== BOX_OK))
                break;
            moveBox(&map[marioPosition->x][marioPosition->y-1],&map[marioPosition->x][marioPosition->y-2]);
            marioPosition->y--;
            break;
        case DOWN:
            if(marioPosition->y+1>NB_BLOCKS_HEIGHT)
                break;
            if(map[marioPosition->x][marioPosition->y+1]==WALL)
                break;
            if ((map[marioPosition->x][marioPosition->y + 1] == BOX || map[marioPosition->x][marioPosition->y +1] == BOX_OK) &&(marioPosition->y + 2 > NB_BLOCKS_HEIGHT || map[marioPosition->x][marioPosition->y + 2] == WALL ||map[marioPosition->x][marioPosition->y + 2] == BOX || map[marioPosition->x][marioPosition->y + 2]== BOX_OK))
                break;
            moveBox(&map[marioPosition->x][marioPosition->y+1],&map[marioPosition->x][marioPosition->y+2]);
            marioPosition->y++;
            break;
        case LEFT:
            if(marioPosition->x-1<0)
                break;
            if(map[marioPosition->x-1][marioPosition->y]==WALL)
                break;
            if ((map[marioPosition->x-1][marioPosition->y] == BOX || map[marioPosition->x-1][marioPosition->y] == BOX_OK) &&(marioPosition->x - 2 < 0 || map[marioPosition->x-2][marioPosition->y] == WALL ||map[marioPosition->x-2][marioPosition->y] == BOX || map[marioPosition->x-2][marioPosition->y]== BOX_OK))
                break;
            moveBox(&map[marioPosition->x-1][marioPosition->y],&map[marioPosition->x-2][marioPosition->y]);
            marioPosition->x--;
            break;
        case RIGHT:
            if(marioPosition->x+1>NB_BLOCKS_WIDTH)
                break;
            if(map[marioPosition->x+1][marioPosition->y]==WALL)
                break;
            if ((map[marioPosition->x+1][marioPosition->y] == BOX || map[marioPosition->x+1][marioPosition->y] == BOX_OK) &&(marioPosition->x + 2 >NB_BLOCKS_WIDTH || map[marioPosition->x+2][marioPosition->y] == WALL ||map[marioPosition->x+2][marioPosition->y] == BOX || map[marioPosition->x+2][marioPosition->y]== BOX_OK))
                break;
            moveBox(&map[marioPosition->x+1][marioPosition->y],&map[marioPosition->x+2][marioPosition->y]);
            marioPosition->x++;
            break;
    }
}

void moveBox(int *firstCase, int *secondCase)
{
    if (*firstCase == BOX || *firstCase == BOX_OK)
    {
        if (*secondCase == GOAL)
            *secondCase = BOX_OK;
        else
            *secondCase = BOX;
        if (*firstCase == BOX_OK)
            *firstCase = GOAL;
        else
            *firstCase = EMPTY;
    }
}


void wellDone(SDL_Surface *screen)
{
    int boolean=1;
    SDL_Event event;
    SDL_Surface *congrats=NULL;
    SDL_Rect congratsPosition;
    congrats=IMG_Load("picture/menu/congrats.png");
    congratsPosition.x=screen->w/2-congrats->w/2;
    congratsPosition.y=screen->h/2-congrats->h/2;
    while(boolean)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_RETURN:
                        boolean=0;
                        break;
                    case SDLK_ESCAPE:
                        boolean=0;
                        break;
                    default:
                        break;
                }
                break;
        }
        SDL_BlitSurface(congrats,NULL,screen,&congratsPosition);
        SDL_Flip(screen);
    }
    SDL_FreeSurface(congrats);
}
