/**
*
*nowlu
*
*@author : Piquenot Ga�tan
*mail : piquenot.gaetan@gmail.com
*
*
*UTILITY: define constant for all the program
*
*/


#include "../inc/editor.h"


void editor(SDL_Surface *screen,int levelNumber)
{
    SDL_Surface *wall = NULL, *box = NULL, *goal = NULL, *mario = NULL;
    SDL_Rect position;
    SDL_Surface *levelDisplay[20]={NULL};
    SDL_Surface *levelBlit=levelDisplay[0];
    SDL_Surface *actualBlock=NULL;
    SDL_Rect actualBlockPosition;
    SDL_Rect levelBlitPosition;
    levelBlitPosition.x=0;
    levelBlitPosition.y=0;
    SDL_Event event;

    int boolean = 1, clickLeftOnGoing = 0, clickRightOnGoing = 0;
    int actualObject = WALL, i = 0, j = 0;
    int map[NB_BLOCKS_WIDTH][NB_BLOCKS_HEIGHT];
    for(i=0;i<NB_BLOCKS_WIDTH;i++)
    {
        for(j=0;j<NB_BLOCKS_HEIGHT;j++)
        {
            map[i][j]=0;
        }
    }

    wall=IMG_Load("picture/sprites/wall.jpg");
    box=IMG_Load("picture/sprites/box.jpg");
    goal=IMG_Load("picture/sprites/goal.png");
    mario=IMG_Load("picture/char/mario_down.gif");
    levelDisplay[0]=IMG_Load("picture/menu/level1.png");
    levelDisplay[1]=IMG_Load("picture/menu/level2.png");
    levelDisplay[2]=IMG_Load("picture/menu/level3.png");
    levelDisplay[3]=IMG_Load("picture/menu/level4.png");
    levelDisplay[4]=IMG_Load("picture/menu/level5.png");
    levelDisplay[5]=IMG_Load("picture/menu/level6.png");
    levelDisplay[6]=IMG_Load("picture/menu/level7.png");
    levelDisplay[7]=IMG_Load("picture/menu/level8.png");
    levelDisplay[8]=IMG_Load("picture/menu/level9.png");
    levelDisplay[9]=IMG_Load("picture/menu/level10.png");
    levelDisplay[10]=IMG_Load("picture/menu/level11.png");
    levelDisplay[11]=IMG_Load("picture/menu/level12.png");
    levelDisplay[12]=IMG_Load("picture/menu/level13.png");
    levelDisplay[13]=IMG_Load("picture/menu/level14.png");
    levelDisplay[14]=IMG_Load("picture/menu/level15.png");
    levelDisplay[15]=IMG_Load("picture/menu/level16.png");
    levelDisplay[16]=IMG_Load("picture/menu/level17.png");
    levelDisplay[17]=IMG_Load("picture/menu/level18.png");
    levelDisplay[18]=IMG_Load("picture/menu/level19.png");
    levelDisplay[19]=IMG_Load("picture/menu/level20.png");

    if(!loadLevel(map,levelNumber))
        exit(EXIT_FAILURE);

    while(boolean)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                boolean = 0;
                break;
            case SDL_MOUSEBUTTONDOWN:
                if(event.button.button == SDL_BUTTON_LEFT)
                {
                    map[event.button.x/SIZE_BLOCK][event.button.y/SIZE_BLOCK]=actualObject;
                    clickLeftOnGoing=1;
                }
                else if (event.button.button == SDL_BUTTON_RIGHT)
                {
                    map[event.button.x / SIZE_BLOCK][event.button.y/SIZE_BLOCK] = EMPTY;
                    clickRightOnGoing = 1;
                }
                break;
            case SDL_MOUSEBUTTONUP:
                if (event.button.button == SDL_BUTTON_LEFT)
                    clickLeftOnGoing = 0;
                else if (event.button.button == SDL_BUTTON_RIGHT)
                    clickRightOnGoing = 0;
                break;
            case SDL_MOUSEMOTION:
                if (clickLeftOnGoing)
                {
                    map[event.motion.x / SIZE_BLOCK][event.motion.y /SIZE_BLOCK] = actualObject;
                }
                else if (clickRightOnGoing)
                {
                    map[event.motion.x / SIZE_BLOCK][event.motion.y /SIZE_BLOCK] = EMPTY;
                }
                switch(actualObject)
                {
                    case WALL:
                        actualBlock=wall;
                        break;
                    case BOX:
                        actualBlock=box;
                        break;
                    case GOAL:
                        actualBlock=goal;
                        break;
                    case MARIO:
                        actualBlock=mario;
                        break;
                }
                actualBlockPosition.x=event.motion.x;
                actualBlockPosition.y=event.motion.y;

                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        boolean = 0;
                        break;
                    case SDLK_s:
                        saveLevel(map,levelNumber);
                        break;
                    case SDLK_c:
                        loadLevel(map,levelNumber);
                        break;
                    case SDLK_a:
                        actualObject = WALL;
                        break;
                    case SDLK_z:
                        actualObject = BOX;
                        break;
                    case SDLK_e:
                        actualObject = GOAL;
                        break;
                    case SDLK_r:
                        actualObject = MARIO;
                        break;
                    case SDLK_PAGEUP:
                        levelNumber +=1;
                        if(levelNumber>20)
                            levelNumber=1;
                        loadLevel(map,levelNumber);
                        break;
                    case SDLK_PAGEDOWN:
                        levelNumber -=1;
                        if(levelNumber<1)
                            levelNumber=20;
                        loadLevel(map,levelNumber);
                    default:
                        break;
                }
                break;
        }
        SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 255, 255, 255));
        for (i = 0 ; i < NB_BLOCKS_WIDTH ; i++)
        {
            for (j = 0 ; j < NB_BLOCKS_HEIGHT ; j++)
            {
                position.x = i * SIZE_BLOCK;
                position.y = j * SIZE_BLOCK;
                switch(map[i][j])
                {
                    case WALL:
                        SDL_BlitSurface(wall, NULL, screen, &position);
                        break;
                    case BOX:
                        SDL_BlitSurface(box, NULL, screen, &position);
                        break;
                    case GOAL:
                        SDL_BlitSurface(goal, NULL, screen, &position);
                        break;
                    case MARIO:
                        SDL_BlitSurface(mario, NULL, screen, &position);
                        break;
                }
            }
        }
        SDL_BlitSurface(actualBlock,NULL,screen,&actualBlockPosition);
        switch(levelNumber)
            {
                case 1:
                    levelBlit=levelDisplay[0];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 2:
                    levelBlit=levelDisplay[1];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 3:
                    levelBlit=levelDisplay[2];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 4:
                    levelBlit=levelDisplay[3];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 5:
                    levelBlit=levelDisplay[4];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 6:
                    levelBlit=levelDisplay[5];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 7:
                    levelBlit=levelDisplay[6];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 8:
                    levelBlit=levelDisplay[7];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 9:
                    levelBlit=levelDisplay[8];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 10:
                    levelBlit=levelDisplay[9];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 11:
                    levelBlit=levelDisplay[10];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 12:
                    levelBlit=levelDisplay[11];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 13:
                    levelBlit=levelDisplay[12];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 14:
                    levelBlit=levelDisplay[13];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 15:
                    levelBlit=levelDisplay[14];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 16:
                    levelBlit=levelDisplay[15];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 17:
                    levelBlit=levelDisplay[16];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 18:
                    levelBlit=levelDisplay[17];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 19:
                    levelBlit=levelDisplay[18];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                case 20:
                    levelBlit=levelDisplay[19];
                    SDL_BlitSurface(levelBlit,NULL,screen,&levelBlitPosition);
                    break;
                default:
                    break;
            }
        SDL_Flip(screen);
    }
    SDL_FreeSurface(wall);
    SDL_FreeSurface(box);
    SDL_FreeSurface(goal);
    SDL_FreeSurface(mario);
    for(i=0;i<20;i++)
        SDL_FreeSurface(levelDisplay[i]);
}
